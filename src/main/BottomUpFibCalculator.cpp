/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file BottomUpFibCalculator.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Luke Johnson <edward212@live.missouristate.edu>
 *          Travis Bushong <pname@missouristate.edu>
 * @brief   Definition of BottomUpFibCalculator.
 */

#include "BottomUpFibCalculator.h"

int BottomUpFibCalculator::nthFibonacciNumber(int n) const {
	int previousFib = 0;
	int currentFib = 1;
	if(n == 0){
		return 1;
	}
	else if(n!=1){ // loop is skipped if n = 1
			for(int i = 0; i<= n-2; i++) {
				int newFib = previousFib + currentFib;
				previousFib = currentFib;
				currentFib = newFib;
			}
	}
	return currentFib;
}
