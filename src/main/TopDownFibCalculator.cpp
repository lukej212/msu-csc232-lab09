/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file TopDownFibCalculator.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Luke Johnson <edward212@live.missouristate.edu>
 *          Partner Name <pname@missouristate.edu>
 * @brief   Definition of TopDownFibCalculator.
 */

#include "TopDownFibCalculator.h"
#include <map>

int TopDownFibCalculator::nthFibonacciNumber(int n) const {
	std::map<int,int> m;
	m[0] = 1;
	m[1] = 1;
	m[2] = 1;
	if((n>m.size()-1) && (n!=0)){
		m[n] = nthFibonacciNumber(n - 1) + nthFibonacciNumber(n - 2);
	}	
	return m[n];
}
